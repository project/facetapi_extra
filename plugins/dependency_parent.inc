<?php

/**
 * @file
 * Performs a dependency check against the parent filter.
 */

/**
 * Adds a dependency on parent.
 */
class FacetapiDependencyParent extends FacetapiDependency {

  /**
   * Executes the dependency check.
   */
  public function execute() {
    if (!empty($this->settings['parent'])) {
      return key_exists($this->settings['parent_value'], $this->activeItems[$this->settings['parent']]);
    }
  }

  /**
   * Adds dependency settings to the form.
   */
  public function settingsForm(&$form, &$form_state) {

    // Builds array of options.
    $options = array('' => t('None'));
    $map = facetapi_get_delta_map();
    foreach (facetapi_get_searcher_info() as $searcher => $info) {
      // Adds blocks for facets that are enabled or whose delta mapping is forced.
      foreach (facetapi_get_delta_map_queue($searcher, 'block') as $facet_name) {
        if ($facet = facetapi_facet_load($facet_name, $searcher)) {
          $options[$facet_name] = $facet['label'];
        }
      }
    }

    $form[$this->id]['parent'] = array(
      '#title' => t('Parent filter'),
      '#type' => 'radios',
      '#options' => $options,
      '#default_value' => $this->settings['parent'],
    );

    $form[$this->id]['parent_value'] = array(
      '#title' => t('Value'),
      '#type' => 'textfield',
      '#default_value' => $this->settings['parent_value'],
      '#description' => t('Set value of the parent facet to display this facet.')
    );
  }

  /**
   * Returns defaults for settings.
   */
  public function getDefaultSettings() {
    return array(
      'parent' => '',
      'parent_value' => '',
    );
  }
}
