Facet API Extra
===============

Dependency plugin Parent
------------------------

This plugin that gives possbility to display facet depending on selected value
of another facet. Example we have three facets (based on taxonomy terms)
Shape (round, square), Radius and Diagonal. We would like to show Radius facet
only in case Shape facet is selected "round" value. We also would like Diagonal
facet to be shown only in case Shape facet selected "square" value. This can be
implemented with Parent dependency plugin implemented in this module. Please
view http://drupal.org/node/1218714 for more information with settings instructions.